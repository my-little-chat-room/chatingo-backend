import convict from 'convict';
import * as dotenv from 'dotenv';

dotenv.config();
dotenv.config({
  path: `.env${process.env.NODE_ENV ? `.${process.env.NODE_ENV}` : ''}`,
});

const convictConfig = convict({
  domainSwagger: {
    doc: 'Domain name for swagger.',
    format: String,
    default: 'localhost:3000',
    env: 'APP_DOMAIN_SWAGGER',
  },
  port: {
    doc: 'The port to bind.',
    format: Number,
    default: 3000,
    env: 'PORT',
  },
  frontendDomainDev: {
    doc: 'Frontend domain for dev',
    format: String,
    default: 'http://localhost:4200',
    env: 'FRONTEND_DOMAIN_DEV',
  },
  pg: {
    doc: 'Postgresql connection string',
    format: String,
    default: 'postgresql://user:password@localhost:5431/db',
    env: 'PG_CONNECTION_STRING',
  },
  databaseName: {
    doc: 'Database name (same as connection string end)',
    format: String,
    default: 'db',
    env: 'DATABASE_NAME',
  },
  cookieSecret: {
    doc: 'Cookie Secret ',
    format: String,
    default: '',
    env: 'COOKIE_KEY',
  },
});

convictConfig.validate({ allowed: 'strict' });

export const config = convictConfig.getProperties();

export type ConfigObject = typeof config;
