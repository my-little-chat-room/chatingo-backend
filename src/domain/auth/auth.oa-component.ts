import { OpenAPIV3 } from 'openapi-types';
import { publicUserOAS } from '../users/users.oa-component';

export const authOATag: OpenAPIV3.TagObject = {
  name: 'auth',
  description: 'Auth description',
};

export const registerUserOAS = {
  tags: ['auth'],
  description: 'Registers new user',
  body: {
    type: 'object',
    properties: {
      name: { type: 'string' },
      password: { type: 'string' },
      email: { type: 'string' },
    },
  },
  response: {
    200: {
      type: 'object',
      properties: {
        status: { type: 'string' },
      },
    },
  },
};
export const loginUserOAS = {
  tags: ['auth'],
  description: 'Log user in',
  body: {
    type: 'object',
    properties: {
      password: { type: 'string' },
      email: { type: 'string' },
    },
  },
  response: {
    200: {
      type: 'object',
      properties: {
        status: { type: 'string' },
      },
    },
  },
};
export const logoutUserOAS = {
  tags: ['auth'],
  description: 'Log user out',
  response: {
    200: {
      type: 'object',
      properties: {
        status: { type: 'string' },
      },
    },
  },
};

export const meUserOAS = {
  tags: ['auth'],
  description: 'Gives back the logged in user',
  user: {
    type: 'object',
    properties: {
      id: { type: 'number' },
    },
  },
  response: {
    200: publicUserOAS,
  },
};
