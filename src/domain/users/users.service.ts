import bcrypt from 'bcrypt';
import { IUser } from './users.interface';
import { UsersRepository } from './users.repository';

export type UsersService = {
  getUsers: () => Promise<IUser[]>;
  getUserById: (id: number) => Promise<IUser>;
  getUserByEmail: (email: string) => Promise<IUser>;
  createUser: (name: string, password: string, email: string) => Promise<IUser>;
};

export const usersService = (usersRepository: UsersRepository): UsersService => {
  return {
    getUsers: async () => {
      return usersRepository.findAll();
    },
    getUserById: async (id: number) => {
      return usersRepository.findById(id);
    },
    getUserByEmail: async (email: string) => {
      return usersRepository.findByEmail(email);
    },
    createUser: async (name: string, password: string, email: string) => {
      const salt = await bcrypt.genSalt(10);
      const passwordHash = await bcrypt.hash(password, salt);

      return usersRepository.create(name, passwordHash, email, salt);
    },
  };
};
