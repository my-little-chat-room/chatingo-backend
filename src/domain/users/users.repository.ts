import { PoolClient } from 'pg';
import { IUser } from './users.interface';

export type UsersRepository = {
  findAll: () => Promise<IUser[]>;
  findById: (id: number) => Promise<IUser>;
  findByEmail: (email: string) => Promise<IUser>;
  create: (name: string, passwordHash: string, email: string, salt: string) => Promise<IUser>;
};

export const usersRepository = (db: PoolClient): UsersRepository => {
  return {
    findAll: async () => {
      const result = await db.query<IUser>('SELECT * FROM users;');
      return result.rows;
    },
    findById: async (id: number) => {
      const result = await db.query<IUser>('SELECT * FROM users WHERE id=$1;', [id]);
      return result.rows[0];
    },
    findByEmail: async (email: string) => {
      const result = await db.query<IUser>('SELECT * FROM users u WHERE u.email=$1;', [email]);
      return result.rows[0];
    },
    create: async (name: string, passwordHash: string, email: string, salt: string) => {
      const result = await db.query<IUser>(
        'INSERT INTO users(name, "passwordHash", email, salt) VALUES ($1, $2, $3, $4) RETURNING *;',
        [name, passwordHash, email, salt],
      );
      return result.rows[0];
    },
  };
};
