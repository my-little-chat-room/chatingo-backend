import { OpenAPIV3 } from 'openapi-types';

export const usersOATag: OpenAPIV3.TagObject = {
  name: 'users',
  description: 'Users description',
};

const fullUserOAS = {
  type: 'object',
  properties: {
    id: { type: 'number' },
    name: { type: 'string' },
    passwordHash: { type: 'string' },
    salt: { type: 'string' },
  },
};

export const publicUserOAS = {
  type: 'object',
  properties: {
    user: {
      type: 'object',
      properties: { id: { type: 'number' }, name: { type: 'string' } },
    },
  },
};

export const getPublicUsersOAS = {
  tags: ['users'],
  description: 'Get all users with public data',
  response: {
    200: {
      type: 'object',
      properties: {
        rooms: {
          type: 'array',
          items: { required: ['id', 'name', 'passwordHash'], additionalProperties: false, ...publicUserOAS },
        },
      },
    },
  },
};

export const getPublicUserOAS = {
  tags: ['users'],
  description: 'Get user public data',
  params: {
    id: { type: 'number' },
  },
  response: {
    200: publicUserOAS,
  },
};

export const getFullUsersOAS = {
  tags: ['users'],
  description: 'Get users with all data in the datebase',
  response: {
    200: {
      type: 'object',
      properties: {
        rooms: {
          type: 'array',
          items: { required: ['id', 'name', 'passwordHash', 'salt'], additionalProperties: false, ...fullUserOAS },
        },
      },
    },
  },
};

export const getFullUserOAS = {
  tags: ['users'],
  description: 'Get user with all data in the database',
  params: {
    id: { type: 'number' },
  },
  response: {
    200: fullUserOAS,
  },
};
