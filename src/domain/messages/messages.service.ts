import { IMessage } from './messages.interface';
import { MessagesRepository } from './messages.repository';

export type MessagesService = {
  getMessages: (roomId: number) => Promise<IMessage[]>;
  getMessage: (id: number, roomId: number) => Promise<IMessage>;
  sendMessage: (roomId: number, data: string) => Promise<IMessage>;
};

export const messagesService = (messagesRepository: MessagesRepository): MessagesService => {
  return {
    getMessages: async (roomId: number) => {
      return messagesRepository.findAll(roomId);
    },
    getMessage: async (id: number, roomId: number) => {
      return messagesRepository.find(id, roomId);
    },
    sendMessage: async (roomId: number, data: string) => {
      return messagesRepository.create(roomId, data);
    },
  };
};
