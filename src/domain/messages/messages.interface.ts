export interface IMessage {
  id: number;
  roomId: number;
  data: string;
  createdAt: Date;
}
