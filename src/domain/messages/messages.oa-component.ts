import { OpenAPIV3 } from 'openapi-types';

export const messagesOATag: OpenAPIV3.TagObject = {
  name: 'messages',
  description: 'Messages description',
};

const messageOAS = {
  type: 'object',
  properties: {
    id: { type: 'number' },
    roomId: { type: 'number' },
    data: { type: 'string' },
    createdAt: { type: 'string', format: 'date' },
  },
};

export const sendMessageOAS = {
  tags: ['messages'],
  description: 'Send new message',
  body: {
    type: 'object',
    properties: {
      message: { type: 'string' },
    },
  },
  params: {
    roomId: { type: 'string' },
  },
  response: {
    200: messageOAS,
  },
};

export const getMessageOAS = {
  tags: ['messages'],
  description: 'Get a message details',
  params: {
    roomId: { type: 'number' },
    messageId: { type: 'number' },
  },
  response: {
    200: messageOAS,
  },
};
export const getMessagesOAS = {
  tags: ['messages'],
  description: 'Get all messages in a room',
  params: {
    roomId: { type: 'string' },
  },
  response: {
    200: {
      type: 'object',
      properties: {
        messages: {
          type: 'array',
          items: messageOAS,
        },
      },
    },
  },
};
