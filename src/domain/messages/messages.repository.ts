import { PoolClient } from 'pg';
import { IMessage } from './messages.interface';

export type MessagesRepository = {
  findAll: (roomId: number) => Promise<IMessage[]>;
  find: (id: number, roomId: number) => Promise<IMessage>;
  create: (roomId: number, data: string) => Promise<IMessage>;
};

export const messagesRepository = (db: PoolClient): MessagesRepository => {
  return {
    findAll: async (roomId: number) => {
      const result = await db.query<IMessage>('SELECT * FROM messages m WHERE m."roomId"=$1;', [roomId]);
      return result.rows;
    },
    find: async (id: number, roomId: number) => {
      const result = await db.query<IMessage>('SELECT * FROM messages m WHERE m."id"=$1 AND m."roomId"=$2;', [
        id,
        roomId,
      ]);
      return result.rows[0];
    },
    create: async (roomId: number, data: string) => {
      const result = await db.query<IMessage>('INSERT INTO messages("roomId", "data") VALUES ($1, $2) RETURNING *;', [
        roomId,
        data,
      ]);
      return result.rows[0];
    },
  };
};
