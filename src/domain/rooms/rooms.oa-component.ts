import { OpenAPIV3 } from 'openapi-types';

export const roomsOATag: OpenAPIV3.TagObject = {
  name: 'rooms',
  description: 'Rooms description',
};

const roomOAS = {
  type: 'object',
  properties: {
    id: { type: 'number' },
    name: { type: 'string' },
    password: { type: 'string' },
  },
};

export const getRoomsOAS = {
  tags: ['rooms'],
  description: 'Give back all rooms with details',
  response: {
    200: {
      type: 'object',
      properties: {
        rooms: {
          type: 'array',
          items: roomOAS,
        },
      },
    },
  },
};

export const createRoomOAS = {
  tags: ['rooms'],
  description: 'Create new room',
  body: {
    type: 'object',
    properties: {
      name: { type: 'string' },
      password: { type: 'string' },
    },
  },
  response: {
    200: roomOAS,
  },
};

export const getRoomOAS = {
  tags: ['rooms'],
  description: 'Give back a room details',
  params: {
    roomId: { type: 'number' },
  },
  response: {
    200: { room: roomOAS },
  },
};
