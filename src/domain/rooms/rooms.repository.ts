import { PoolClient } from 'pg';
import { IRoom } from './rooms.interface';

export type RoomsRepository = {
  findAll: () => Promise<IRoom[]>;
  find: (id: number) => Promise<IRoom>;
  create: (name: string, password?: string) => Promise<IRoom>;
};

export const roomsRepository = (db: PoolClient): RoomsRepository => {
  return {
    findAll: async () => {
      const result = await db.query<IRoom>('SELECT * FROM rooms;');
      return result.rows;
    },
    find: async (id: number) => {
      const result = await db.query<IRoom>('SELECT * FROM rooms WHERE id=$1;', [id]);
      return result.rows[0];
    },
    create: async (name: string, password?: string) => {
      const result = await db.query<IRoom>('INSERT INTO rooms(name, password) VALUES ($1, $2) RETURNING * ;', [
        name,
        password,
      ]);
      console.log(result.rows[0]);
      return result.rows[0];
    },
  };
};
