import { IRoom } from './rooms.interface';
import { RoomsRepository } from './rooms.repository';

export type RoomsService = {
  getRooms: () => Promise<IRoom[]>;
  getRoom: (id: number) => Promise<IRoom>;
  createRoom: (name: string, password?: string) => Promise<IRoom>;
};

export const roomsService = (roomsRepository: RoomsRepository): RoomsService => {
  return {
    getRooms: async () => {
      return roomsRepository.findAll();
    },
    getRoom: async (id: number) => {
      return roomsRepository.find(id);
    },
    createRoom: async (name: string, password?: string) => {
      return roomsRepository.create(name, password);
    },
  };
};
