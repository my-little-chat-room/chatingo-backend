import { Server } from 'socket.io';
import { config } from './config';
import { authOATag } from './domain/auth/auth.oa-component';
import { messagesOATag } from './domain/messages/messages.oa-component';
import { roomsOATag } from './domain/rooms/rooms.oa-component';
import { usersOATag } from './domain/users/users.oa-component';
import { fastifyServerFactory } from './framework/fastify/fastify-server.factory';
import { fastifySwaggerFactory } from './framework/fastify/fastify-swagger.factory';
import { createSocket } from './framework/socket/socket.factory';

export const start = async (): Promise<void> => {
  const tags = [roomsOATag, messagesOATag, usersOATag, authOATag];

  const swaggerOptions = fastifySwaggerFactory({
    tags,
    host: config.domainSwagger,
  });

  const server = await fastifyServerFactory({
    swaggerOptions,
    allowedOrigins: [config.frontendDomainDev],
  });

  void server.setErrorHandler((_error, _request, reply) => {
    void reply.status(500).send({ errorCode: 'INTERNAL_SERVER_ERROR' });
  });

  void server.oas();
  await server.ready().then(() => {
    const io = server.io as Server;
    createSocket(io);
  });
  void server.listen(config.port, '0.0.0.0');
};
