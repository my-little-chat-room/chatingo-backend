import path from 'path';
import { PoolClient } from 'pg';
import { createDb, migrate } from 'postgres-migrations';
import { config } from '../../config';

export const startMigrationFactory = async (client: PoolClient): Promise<void> => {
  const databaseName = config.databaseName;
  try {
    await createDb(databaseName, { client });
    await migrate({ client }, path.join(__dirname, './', 'migrations'));
  } catch (e) {
    console.error(e);
    process.exit(1);
  }
};
