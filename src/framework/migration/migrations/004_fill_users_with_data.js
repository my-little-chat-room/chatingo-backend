const bcrypt = require('bcrypt');
const dotenv = require('dotenv');

dotenv.config();

const password = process.env.FIRST_USER_PASSWORD;
const userName = process.env.FIRST_USER_NAME;
const email = process.env.FIRST_USER_EMAIL;

const salt = process.env.FIRST_USER_SALT;
const passwordHash = bcrypt.hashSync(password, salt);
module.exports.generateSql = () => `
  INSERT INTO
  users("name", "email","salt","passwordHash")
  VALUES
  ('${userName}', '${email}', '${salt}', '${passwordHash}')
  ON CONFLICT (email) DO NOTHING;
`;
