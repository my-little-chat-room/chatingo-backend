import { FastifySchema } from 'fastify';

export enum EEndpointMethod {
  GET = 'GET',
  POST = 'POST',
  PATCH = 'PATCH',
  PUT = 'PUT',
  DELETE = 'DELETE',
}

export enum EStatusMessages {
  LOGGED_IN = 'Logged in',
  LOGGED_OUT = 'Logged out',
  REGISTRATION_SUCCESS = 'Registration Done!',
}

export type RouteSchema = FastifySchema & { tags: string[]; summary?: string; description?: string };
