import { Server } from 'socket.io';
import { loggedInUserFactory } from './logged-in-user.factory';

export const createSocket = (io: Server): void => {
  io.on('connect', socket => {
    console.log('client connect');

    loggedInUserFactory(socket);

    socket.on('disconnect', () => {
      console.log('client disconnect');
    });
  });
};
