import { Socket } from 'socket.io';

export const loggedInUserFactory = (socket: Socket): void => {
  const users: Record<string, number> = {};
  socket.on('login', (userId: number) => {
    users[socket.id] = userId;
    socket.emit('online-users', { users });
  });
  socket.on('logout', () => {
    delete users[socket.id];
    socket.emit('online-users', { users });
  });
  socket.on('disconnect', () => {
    delete users[socket.id];
    socket.emit('online-users', { users });
  });
};
