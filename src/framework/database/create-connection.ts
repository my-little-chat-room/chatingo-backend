import { Pool, PoolClient } from 'pg';
import { config } from '../../config';

export const startDbConnect = async (): Promise<PoolClient> => {
  const pool = new Pool({ connectionString: config.pg });
  const client = await pool.connect();

  return client;
};
