import { FastifyOASOptions } from 'fastify-oas';
import { OpenAPIV3 } from 'openapi-types';

export interface IFastifySwaggerFactoryArgs {
  host: string;
  tags: OpenAPIV3.TagObject[];
}

export const fastifySwaggerFactory = (factoryArgs: IFastifySwaggerFactoryArgs): FastifyOASOptions => ({
  routePrefix: '/documentation',
  exposeRoute: true,
  swagger: {
    info: {
      title: 'Chatingo',
      description: 'Api documentation',
      version: '0.1.0',
    },
    host: factoryArgs.host,
    schemes: ['http'],
    consumes: ['application/json'],
    produces: ['application/json'],
    tags: factoryArgs.tags,
  },
});
