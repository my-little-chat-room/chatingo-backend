import bcrypt from 'bcrypt';
import fastify, { FastifyInstance } from 'fastify';
import fastifyCors from 'fastify-cors';
import fastifyHelmet from 'fastify-helmet';
import fastifyOAS, { FastifyOASOptions } from 'fastify-oas';
import passport from 'fastify-passport';
import fastifySecureSession from 'fastify-secure-session';
import fastifySocketIO from 'fastify-socket.io';
import passportLocal from 'passport-local';
import { config } from '../../config';
import { loginUserOAS, logoutUserOAS, meUserOAS, registerUserOAS } from '../../domain/auth/auth.oa-component';
import { getMessageOAS, getMessagesOAS, sendMessageOAS } from '../../domain/messages/messages.oa-component';
import { messagesRepository } from '../../domain/messages/messages.repository';
import { messagesService } from '../../domain/messages/messages.service';
import { createRoomOAS, getRoomOAS, getRoomsOAS } from '../../domain/rooms/rooms.oa-component';
import { roomsRepository } from '../../domain/rooms/rooms.repository';
import { roomsService } from '../../domain/rooms/rooms.service';
import { IUser } from '../../domain/users/users.interface';
import { usersRepository } from '../../domain/users/users.repository';
import { usersService } from '../../domain/users/users.service';
import { loginEndpointFactory } from '../../endpoints/auth/login.endpoint';
import { logoutEndpointFactory } from '../../endpoints/auth/logout.endpoint';
import { meEndpointFactory } from '../../endpoints/auth/me.endpoint';
import { registerEndpointFactory } from '../../endpoints/auth/register.endpoint';
import { getMessageEndpointFactory } from '../../endpoints/message/get-message.endpoint';
import { getMessagesEndpointFactory } from '../../endpoints/message/get-messages.endpoint';
import { sendMessageEndpointFactory } from '../../endpoints/message/send-message.endpoint';
import { createRoomEndpointFactory } from '../../endpoints/room/create-room.endpoint';
import { getRoomEndpointFactory } from '../../endpoints/room/get-room.endpoint';
import { getRoomsEndpointFactory } from '../../endpoints/room/get-rooms.endpoint';
import { startDbConnect } from '../database/create-connection';
import { startMigrationFactory } from '../migration/migration-factory';

const MAX_AGE = 604800000;

export interface IFastifyFactoryArgs {
  swaggerOptions: FastifyOASOptions;
  allowedOrigins: (string | RegExp)[];
}

export const fastifyServerFactory = async (factoryArgs: IFastifyFactoryArgs): Promise<FastifyInstance> => {
  const db = await startDbConnect();

  await startMigrationFactory(db);

  const app = fastify({ logger: true });
  await app.register(fastifyHelmet);

  app.setErrorHandler((error, _, reply) => {
    void reply.status(500).send({
      errorCode: 'INTERNAL_SERVER_ERROR',
      ...error,
    });
  });

  await app.register(fastifyCors, {
    origin: factoryArgs.allowedOrigins,
    methods: ['GET', 'POST', 'PUT', 'PATCH', 'DELETE'],
    credentials: true,
  });
  await app.register(fastifySocketIO, { cors: { origin: '*' } });

  await app.register(fastifyOAS, factoryArgs.swaggerOptions);

  //REPOSITORYS
  const messagesReposotory = messagesRepository(db);
  const roomsReposotory = roomsRepository(db);
  const usersReposotory = usersRepository(db);

  //SERVICES
  const roomService = roomsService(roomsReposotory);
  const messageService = messagesService(messagesReposotory);
  const userService = usersService(usersReposotory);

  passport.use(
    'local',
    new passportLocal.Strategy(
      {
        usernameField: 'email',
        passwordField: 'password',
      },
      // eslint-disable-next-line @typescript-eslint/no-misused-promises
      async (email, password, cb) => {
        const user = await userService.getUserByEmail(email);
        console.log(email, user);
        if (!user) {
          return cb(null, false, { message: 'Insufficient Permission.' });
        }
        const { salt, passwordHash } = user;
        const comparePassword = await bcrypt.hash(password, salt).catch(() => null);
        if (passwordHash !== comparePassword) {
          return cb(null, false, { message: 'Insufficient Permission.' });
        }
        return cb(null, user);
      },
    ),
  );

  void passport.registerUserSerializer(async (user: IUser) => user.id);
  void passport.registerUserDeserializer(async (id: number) => userService.getUserById(id));

  void app.register(fastifySecureSession, {
    key: Buffer.from(config.cookieSecret, 'hex'),
    cookie: { path: '/', maxAge: MAX_AGE },
    cookieName: 'chat-session',
  });

  void app.register(passport.initialize());
  void app.register(passport.secureSession());

  //ENDPOINTS
  getRoomsEndpointFactory(app, getRoomsOAS, roomService);
  getRoomEndpointFactory(app, getRoomOAS, roomService);
  createRoomEndpointFactory(app, createRoomOAS, roomService);
  sendMessageEndpointFactory(app, sendMessageOAS, messageService);
  getMessagesEndpointFactory(app, getMessagesOAS, messageService);
  getMessageEndpointFactory(app, getMessageOAS, messageService);
  registerEndpointFactory(app, registerUserOAS, userService);
  loginEndpointFactory(app, loginUserOAS, userService);
  logoutEndpointFactory(app, logoutUserOAS);
  meEndpointFactory(app, meUserOAS, userService);

  return app;
};
