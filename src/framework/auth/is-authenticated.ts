import passport from 'fastify-passport';

export const isAuthenticated = passport.authenticate('local', async (request, reply) => {
  if (request.user) {
    return Promise.resolve();
  }
  return reply.status(401).send({ error: 'Unauthorized' });
});
