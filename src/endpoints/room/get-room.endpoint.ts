import { FastifyInstance } from 'fastify';
import { IRoom } from '../../domain/rooms/rooms.interface';
import { RoomsService } from '../../domain/rooms/rooms.service';
import { isAuthenticated } from '../../framework/auth/is-authenticated';
import { EEndpointMethod, RouteSchema } from '../../framework/endpoint/endpoint.interface';

export interface IGetRoomOptions {
  Params: { roomId: number };
}

export interface IGetRoomOutput extends IRoom {}

export const getRoomEndpointFactory = (
  server: FastifyInstance,
  schema: RouteSchema,
  roomsService: RoomsService,
): void => {
  server.route<IGetRoomOptions>({
    method: EEndpointMethod.GET,
    url: '/api/rooms/:roomId',
    schema: schema,
    preValidation: isAuthenticated,
    handler: async (request, reply) => {
      const params = request.params;
      const room = await roomsService.getRoom(params.roomId);
      void reply.send({ room: room });
    },
  });
};
