import { FastifyInstance } from 'fastify';
import { Server } from 'socket.io';
import { IRoom } from '../../domain/rooms/rooms.interface';
import { RoomsService } from '../../domain/rooms/rooms.service';
import { isAuthenticated } from '../../framework/auth/is-authenticated';
import { EEndpointMethod, RouteSchema } from '../../framework/endpoint/endpoint.interface';

export interface ICreateRoomRequest {
  Body: { name: string; password?: string };
}
export interface ICreateRoomReply {
  Reply: ICreateRoomOutput;
}
export interface ICreateRoomInput {
  name: string;
  password?: string;
}
export interface ICreateRoomOutput extends IRoom {}

export const createRoomEndpointFactory = (
  server: FastifyInstance,
  schema: RouteSchema,
  roomsService: RoomsService,
): void => {
  server.route<ICreateRoomRequest, ICreateRoomReply>({
    method: EEndpointMethod.POST,
    url: '/api/rooms',
    schema: schema,
    preValidation: isAuthenticated,
    handler: async function (request, reply) {
      const body = request.body;
      console.log(request);
      const room = await roomsService.createRoom(body.name, body.password);
      const io = server.io as Server;
      io.emit('new-room', { room });
      void reply.send(room);
    },
  });
};
