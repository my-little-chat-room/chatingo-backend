import { FastifyInstance } from 'fastify';
import { IRoom } from '../../domain/rooms/rooms.interface';
import { RoomsService } from '../../domain/rooms/rooms.service';
import { isAuthenticated } from '../../framework/auth/is-authenticated';
import { EEndpointMethod, RouteSchema } from '../../framework/endpoint/endpoint.interface';
import { IGetMessageReply } from '../message/get-message.endpoint';

export interface IGetRoomsRequest {}
export interface IGetRoomsReply {
  Reply: IGetRoomsOutput;
}

export interface IGetRoomsOutput extends IRoom {}

export const getRoomsEndpointFactory = (
  server: FastifyInstance,
  schema: RouteSchema,
  roomsService: RoomsService,
): void => {
  server.route<IGetRoomsRequest, IGetMessageReply>({
    method: EEndpointMethod.GET,
    url: '/api/rooms',
    schema: schema,
    preValidation: isAuthenticated,
    handler: async (_request, reply) => {
      const rooms = await roomsService.getRooms();
      void reply.send({ rooms: rooms });
    },
  });
};
