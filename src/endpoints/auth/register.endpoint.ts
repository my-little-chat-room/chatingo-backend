import { FastifyInstance } from 'fastify';
import { UsersService } from '../../domain/users/users.service';
import { EEndpointMethod, EStatusMessages, RouteSchema } from '../../framework/endpoint/endpoint.interface';

export interface IRegisterRequest {
  Body: IRegisterInput;
}
export interface IRegisterReply {
  Reply: IRegisterOutput;
}
export interface IRegisterInput {
  email: string;
  password: string;
  name: string;
}
export interface IRegisterOutput {
  status: EStatusMessages.REGISTRATION_SUCCESS;
}

export const registerEndpointFactory = (
  server: FastifyInstance,
  schema: RouteSchema,
  usersService: UsersService,
): void => {
  server.route<IRegisterRequest, IRegisterReply>({
    method: EEndpointMethod.POST,
    url: '/api/auth/register',
    schema: schema,
    handler: async (request, reply) => {
      const { email, password, name } = request.body;
      await usersService.createUser(name, password, email);
      void reply.send({ status: EStatusMessages.REGISTRATION_SUCCESS });
    },
  });
};
