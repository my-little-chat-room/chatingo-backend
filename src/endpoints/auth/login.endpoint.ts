import { FastifyInstance } from 'fastify';
import passport from 'fastify-passport';
import { UsersService } from '../../domain/users/users.service';
import { EEndpointMethod, EStatusMessages, RouteSchema } from '../../framework/endpoint/endpoint.interface';

export interface ILoginOptions {
  Body: { email: string; password: string };
}

export const loginEndpointFactory = (server: FastifyInstance, schema: RouteSchema, userService: UsersService): void => {
  server.route<ILoginOptions>({
    method: EEndpointMethod.POST,
    url: '/api/auth/login',
    schema: schema,
    preHandler: passport.authenticate('local', { authInfo: true }),
    handler: async (_request, reply) => {
      void reply.send({ status: EStatusMessages.LOGGED_IN });
    },
  });
};
