import { FastifyInstance } from 'fastify';
import { IUser } from '../../domain/users/users.interface';
import { UsersService } from '../../domain/users/users.service';
import { isAuthenticated } from '../../framework/auth/is-authenticated';
import { EEndpointMethod, RouteSchema } from '../../framework/endpoint/endpoint.interface';

export interface IMeRequest {}
export interface IMeReply {
  Reply: {
    user: IUser;
  };
}

export const meEndpointFactory = (server: FastifyInstance, schema: RouteSchema, userService: UsersService): void => {
  server.route<IMeRequest, IMeReply>({
    method: EEndpointMethod.GET,
    schema,
    url: '/api/auth/me',
    preValidation: isAuthenticated,
    handler: async (request, reply): Promise<void> => {
      const { id } = request.user as IUser;
      const user = await userService.getUserById(id);
      void reply.send({ user: user });
    },
  });
};
