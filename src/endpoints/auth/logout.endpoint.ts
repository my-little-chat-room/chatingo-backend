import { FastifyInstance } from 'fastify';
import { EEndpointMethod, EStatusMessages, RouteSchema } from '../../framework/endpoint/endpoint.interface';

export interface ILogoutRequest {
  Body: ILogoutInput;
}
export interface ILogoutReply {
  Reply: ILogoutOutput;
}
export interface ILogoutInput {}
export interface ILogoutOutput {
  status: EStatusMessages.LOGGED_OUT;
}

export const logoutEndpointFactory = (server: FastifyInstance, schema: RouteSchema): void => {
  server.route<ILogoutRequest, ILogoutReply>({
    method: EEndpointMethod.POST,
    url: '/api/auth/logout',
    schema: schema,
    handler: async (request, reply) => {
      request.session.delete();
      void reply.send({ status: EStatusMessages.LOGGED_OUT });
    },
  });
};
