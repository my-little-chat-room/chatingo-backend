import { FastifyInstance } from 'fastify';
import { IMessage } from '../../domain/messages/messages.interface';
import { MessagesService } from '../../domain/messages/messages.service';
import { isAuthenticated } from '../../framework/auth/is-authenticated';
import { EEndpointMethod, RouteSchema } from '../../framework/endpoint/endpoint.interface';

export interface IGetMessageRequest {
  Params: {
    roomId: number;
    messageId: number;
  };
}
export interface IGetMessageReply {
  Reply: IGetMessageOutput;
}

export interface IGetMessageOutput extends IMessage {}

export const getMessageEndpointFactory = (
  server: FastifyInstance,
  schema: RouteSchema,
  messagesService: MessagesService,
): void => {
  server.route<IGetMessageRequest, IGetMessageReply>({
    method: EEndpointMethod.GET,
    url: '/api/rooms/:roomId/messages/:messageId',
    schema: schema,
    preValidation: isAuthenticated,
    handler: async (request, reply) => {
      const params = request.params;
      const message = await messagesService.getMessage(params.messageId, params.roomId);
      void reply.send(message);
    },
  });
};
