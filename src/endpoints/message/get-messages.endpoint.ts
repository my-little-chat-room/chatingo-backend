import { FastifyInstance } from 'fastify';
import { IMessage } from '../../domain/messages/messages.interface';
import { MessagesService } from '../../domain/messages/messages.service';
import { isAuthenticated } from '../../framework/auth/is-authenticated';
import { EEndpointMethod, RouteSchema } from '../../framework/endpoint/endpoint.interface';

export interface IGetMessagesRequest {
  Params: {
    roomId: number;
  };
}

export interface IGetMessagesReply {
  Reply: IGetMessagesOutput;
}

export interface IGetMessagesOutput {
  messages: IMessage[];
}

export const getMessagesEndpointFactory = (
  server: FastifyInstance,
  schema: RouteSchema,
  messagesService: MessagesService,
): void => {
  server.route<IGetMessagesRequest, IGetMessagesReply>({
    method: EEndpointMethod.GET,
    url: '/api/rooms/:roomId/messages',
    schema: schema,
    preValidation: isAuthenticated,
    handler: async (request, reply) => {
      const params = request.params;
      const messages = await messagesService.getMessages(params.roomId);
      void reply.send({ messages: messages });
    },
  });
};
