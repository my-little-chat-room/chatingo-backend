import { FastifyInstance } from 'fastify';
import { Server } from 'socket.io';
import { MessagesService } from '../../domain/messages/messages.service';
import { isAuthenticated } from '../../framework/auth/is-authenticated';
import { EEndpointMethod, RouteSchema } from '../../framework/endpoint/endpoint.interface';

export interface ISendMessageOptions {
  Body: { data: string };
  Params: {
    roomId: number;
  };
}

export const sendMessageEndpointFactory = (
  server: FastifyInstance,
  schema: RouteSchema,
  messagesService: MessagesService,
): void => {
  server.route<ISendMessageOptions>({
    method: EEndpointMethod.POST,
    url: '/api/rooms/:roomId/messages',
    schema: schema,
    preValidation: isAuthenticated,
    handler: async (request, reply) => {
      const body = request.body;
      const params = request.params;
      const message = await messagesService.sendMessage(params.roomId, body.data);
      const io = server.io as Server;
      io.emit(`${params.roomId}-new-message`, { message: body.data });
      void reply.send(message);
    },
  });
};
