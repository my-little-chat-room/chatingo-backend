from node:12 as deps

WORKDIR /app

COPY package.json yarn.lock ./

RUN yarn --frozen-lockfile

COPY src src

COPY .prettierrc tsconfig.json .eslintrc.js ./

RUN yarn generate-secret

RUN yarn build

EXPOSE 3000

CMD [ "yarn", "start" ]

